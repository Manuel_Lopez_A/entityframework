﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraforMars_Def.Models;

namespace TerraforMars_Def.Data
{
    public class TerraforMarsContext : DbContext
    {
        private const string connection = "Server=(localdb)\\mssqllocaldb;Database=TerraFormars;Trusted_Connection=True;";

        public DbSet<Player> Players { get; set; }
        public DbSet<Corporation> Corporations { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Maker> Makers { get; set; }
        //public DbSet<PlayerGames> PlayerGames { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connection);
        }

        // relaciones
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // MANY TO MANY
            // un Maker tiene muchos vecinos makers
            modelBuilder.Entity<Maker>()
                .HasMany(p => p.MakersVecinos)
                .WithMany()
                .UsingEntity(j => j.ToTable("CasillasVecinas"));

            // MANY TO MANY
            // un Player tiene muchos Games y un Game tiene muchos Player
            modelBuilder.Entity<Player>()
                .HasMany(p => p.Games)
                .WithMany(g => g.Players)
                .UsingEntity(j => j.ToTable("PlayerGamesList"));

            // ONE TO ONE
            // Un Player tiene una Corporation y una Corporation tiene un Player
            modelBuilder.Entity<Player>()
                .HasOne(p => p.Corporation)
                .WithOne(c => c.Player)
                .HasForeignKey<Corporation>(c => c.PlayerId)
                .OnDelete(DeleteBehavior.NoAction);

            // ONE TO MANY
            // un Game tiene un Winner(Player) y un Player tiene varios WonGames (Game)
            modelBuilder.Entity<Game>()
                .HasOne(g => g.Winner)
                .WithMany(p => p.WonGames)
                .HasForeignKey(g => g.WinnerId) // Especifica la clave externa en Game (WinnerId)
                .OnDelete(DeleteBehavior.NoAction);
            // ONE TO MANY
            // un Maker tiene una CorpoAsociada(Corporation) y una Corporation tiene varios MakersList (Maker)
            modelBuilder.Entity<Maker>()
                .HasOne(m => m.CorpoAsociada)
                .WithMany(c => c.MakersList)
                .HasForeignKey(m => m.CorpoId) 
                .OnDelete(DeleteBehavior.NoAction);
            
        }
    }

    
}
