﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraforMars_Def.Data;

namespace TerraforMars_Def.Models
{
    public class Maker
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdMaker { get; set; }
        public string Name { get; set; } = string.Empty;
        public int MaxNeighbours { get; set; } = 3;
        public Typemaker Typemaker { get; set; }
        public ISet<Maker> MakersVecinos { get; set; } = new HashSet<Maker>();
        //public Corporation Corporation;
        public int? CorpoId { get; set; }
        public Corporation? CorpoAsociada { get; set; }

        public Maker() { }
        public Maker(string name)
        {
            Name = name;

            Random r = new Random();
            int randomNumber = r.Next(3);

            switch (randomNumber)
            {
                case 0:
                    Typemaker = Typemaker.CIUTAT;
                    break;
                case 1:
                    Typemaker = Typemaker.JUNGLA;
                    break;
                default:
                    Typemaker = Typemaker.OCEA;
                    break;
            }
        }
        public void hola()
        {
            Console.WriteLine("hola");
        }

        public List<Maker> GetMakersByType(Typemaker t, TerraforMarsContext context)
        {
            List<Maker> makersByType = [];
            foreach (Maker m in context.Makers)
            {
                if(m.Typemaker == t)
                    makersByType.Add(m);
            }
            return makersByType;
        }
        public List<Maker> GetMakersByTypeNoCorporation(Typemaker t, TerraforMarsContext context)
        {
            List<Maker> lista = GetMakersByType(t, context);
            List<Maker> listaFiltrada = new List<Maker>();
            foreach (Maker m in lista) 
            {
                if (m.CorpoAsociada == null )
                    listaFiltrada.Add(m);
            }
            return listaFiltrada;
        }
        public List<Maker> getMakersByTypeCorporation(Typemaker t, TerraforMarsContext context)
        {
            List<Maker> lista = GetMakersByType(t, context);
            List<Maker> listaFiltrada = new List<Maker>();
            foreach (Maker m in lista)
            {
                if (m.CorpoAsociada != null)
                    listaFiltrada.Add(m);
            }
            return listaFiltrada;
        }


    }
}

