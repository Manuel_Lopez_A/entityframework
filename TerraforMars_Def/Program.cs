﻿using TerraforMars_Def.Data;
using TerraforMars_Def.Models;

Console.WriteLine("----------TERRAFORMARS----------");

using (var context = new TerraforMarsContext())
{
    // Creates the database if not exists 
    context.Database.EnsureCreated();

    Game game = new Game();
    context.Games.Add(game);

    CreateTable(context);
    Queue<Player> players = CreatePlayers(context, game);
    Console.WriteLine($"Tenemos una lista con {players.Count} jugadores.");
    context.SaveChanges();

    Jugar(game, players, context);

    context.SaveChanges();
}
static List<int> RollingDices()
{
    Random r = new Random();
    List<int> tiradas = new();
    for (int i = 0; i < 6; i++)
    {
        tiradas.Add(r.Next(1,7));
    }
    return tiradas;
}
void ResolveDices(Player p, List<int> tirades, Game game, TerraforMarsContext context)
{
    Console.WriteLine(string.Join(", ", tirades));

    Maker mAuxiliar = new();

    int temperatura = 0;
    int oxigeno = 0;
    int oceano = 0;
    int jungla = 0;
    int ciudad = 0;
    int victoria = 0;

    foreach (int tirada in tirades)
    {
        switch (tirada)
        {
            case 1:
                temperatura++;
                break;
            case 2:
                oxigeno++;
                break;
            case 3:
                oceano++;
                break;
            case 4:
                jungla++;
                break;
            case 5:
                ciudad++;
                break;
            default:
                victoria++;
                break;
        }
    }
    if (temperatura >= 3)
    {
        game.Temperature += 2;
        Console.WriteLine($"Incrementamos la temperatura a :{game.Temperature}");
    }
    if (oxigeno >= 3)
    {
        game.Oxygen += 1;
        Console.WriteLine($"Incrementamos el oxygen a :{game.Oxygen}");
    }
    if (victoria >= 3)
    {
        p.Corporation.VictoryPoints += 2;
        Console.WriteLine($"Incrementamos puntos de victoria de{p.Name} a :{p.Corporation.VictoryPoints}");
    }
    if (oceano >= 3)
    {
        //Console.WriteLine("OCEANO >= 3");
        List<Maker> oceansSenseCorp = mAuxiliar.GetMakersByTypeNoCorporation(Typemaker.OCEA, context);
        if (oceansSenseCorp.Count == 0)
        {
            Console.WriteLine("NO hay ninguna casilla de océano libre");
        }
        else
        {
            Console.WriteLine("OCÉANO");
            oceansSenseCorp[0].CorpoAsociada = p.Corporation;
        }
    }
    if (ciudad >= 3)
    {
        List<Maker> ciudadWithoutCorp = mAuxiliar.GetMakersByTypeNoCorporation(Typemaker.CIUTAT, context);
        if (ciudadWithoutCorp.Count == 0)
        {
            Console.WriteLine("NO hay ninguna casilla de ciudad libre");
        }
        else
        {
            Console.WriteLine("CIUDAD");
            ciudadWithoutCorp[0].CorpoAsociada = p.Corporation;
        }
    }
    if (jungla >= 3)
    {
        List<Maker> junglaWithoutCorp = mAuxiliar.GetMakersByTypeNoCorporation(Typemaker.JUNGLA, context);
        if (junglaWithoutCorp.Count == 0)
        {
            Console.WriteLine("NO hay ninguna casilla de jungla libre");
        }
        else
        {
            Console.WriteLine("JUNGLA");
            junglaWithoutCorp[0].CorpoAsociada = p.Corporation;
        }
    }
}


void Jugar(Game g, Queue<Player>players, TerraforMarsContext context)
{
    bool play_on = true;
    while(play_on)
    {
        Player player = players.Dequeue();
        Console.WriteLine($"Turno de {player.Name}");
        ResolveDices(player, RollingDices(), g, context);
        players.Enqueue(player);
        if (g.IsEndedGame(context))
        {
            play_on = false;
            SetWinnerGame(g, context);
        }
    }
}

void SetWinnerGame(Game g, TerraforMarsContext context)
{
    Player winner = null;
    int max = 0;
    foreach (Corporation c in context.Corporations)
    {
        if(c.VictoryPoints > max)
        {
            max = c.VictoryPoints;
            winner = c.Player;
        }
    }
    g.Winner = winner;
    g.DateEnd = DateTime.Now;
    winner.Wins+=1;

}
void CreateTable(TerraforMarsContext context)
{
    TableUtils tu = new(context);
    List<List<Maker>> tabla = tu.CreateTable(9, 5);
    tu.PrintTable(tabla);    
}

  /// CREAMOS: 
  /// - 3 players
  /// - 3 Corporation
  /// [+] le añadimos a los players las corporations y el Game 
Queue<Player> CreatePlayers(TerraforMarsContext context, Game game)
{
    Player player1 = new() { Name = "Manuel", Wins = 0 };
    Player player2 = new() { Name = "Darío", Wins = 0 };
    Player player3 = new() { Name = "Laura", Wins = 0 };
    context.Players.Add(player1);
    context.Players.Add(player2);
    context.Players.Add(player3);

    foreach (Player p in context.Players)
    {
        Console.WriteLine($"First Name: {p.Name}, Wins: {p.Wins}");
    }

    Corporation corporation1 = new() { Name = "Credicor", Description = "descripcion Credicorp" };
    Corporation corporation2 = new() { Name = "Ecoline", Description = "descripcion Ecoline" };
    Corporation corporation3 = new() { Name = "Helion", Description = "descripcion Helion" };
    context.Corporations.Add(corporation1);
    context.Corporations.Add(corporation2);
    context.Corporations.Add(corporation3);

    player1.Corporation = corporation1;
    player2.Corporation = corporation2;
    player3.Corporation = corporation3;

    player1.Games.Add(game);
    player2.Games.Add(game);
    player3.Games.Add(game);

    // RETORNA LA LISTA DE JUGADORES
    Queue<Player> queue = new Queue<Player>();
    queue.Enqueue(player1);
    queue.Enqueue(player2);
    queue.Enqueue(player3);
    return queue;
}




